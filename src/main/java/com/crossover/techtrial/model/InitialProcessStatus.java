package com.crossover.techtrial.model;

/**
 * Available statuses for initial process.
 *
 * @author Wasif Islam
 */
public enum InitialProcessStatus {
    STARTED,
    FINISHED,
    FAILED
}
